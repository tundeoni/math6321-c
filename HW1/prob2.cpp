/*
 * prob2.cpp
 * 
 * Copyright 2016 Tunde Oni<boni@smu.edu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cmath>
#include <math.h>
#include <iomanip>
using namespace std;

int main()
{
	
	int i;
	double x,x1;
	double f,dfdx;
	const double epsilon = 0.00000001; //tolerance
	const int n = 15; //number of steps
	
  cout << "Enter starting value x: "; 
  cin >> x1; 
  
  for(i = 1; i <= n; i++)
  {
    x = x1; 
	f = ((x*x*x)-(3*x*x)+((108.0/36.0)*x)-(1.0/6.0)); 
    dfdx = ((3*x*x)-(6*x)+(108.0/36.0)); 
	x1 = x - f/dfdx; //Newton Method 
	cout << i << " " << x1 << "\n"; 
  }
  
  
    while(abs(x-x1) < epsilon) 
	{
	  cout << "\n" << "The root is " << x1 << "."; //displays the root
	  return 0;
    }
	
		
	
	return 0;
}

