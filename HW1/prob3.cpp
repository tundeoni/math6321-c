/*
 * prob3.cpp
 * 
 * Copyright 2016 Tunde Oni <boni@smu.edu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cmath>
#include <math.h>
#include <iomanip>
#include "matrix.cpp"
using namespace std;

int main()
{
	
	int i;
	double x,x1,y,y1;
	double f,dfdx,dfdy,g,dgdx,dgdy;
	const double absolute= 0.0000000001; //absolute tolerance
	const double relative= 0.000001;
	const double epsilon = abs(absolute - relative);
	const int n = 15; //number of steps
	Matrix ini_vals = Matrix(2,1);
	Matrix jacb = Matrix(2,2);
	Matrix inv_jacob = Matrix(2,2);
	
	
	
  cout << "Enter starting value x: "; 
  cin >> x1; 
  cout << "Enter starting value y: "; 
  cin >> y1;
  
  for(i = 1; i <= n; i++)
  {
    x = x1;
    y = y1; 
    
    //functions
	f = (x*x) + (y*y) -4.0; 
    dfdx = (2.0)*x;
    dfdy = (2.0)*y;
    g =  (x*y) - 1;
    dgdx = y;
    dgdy =x;
    
    //Matrices and Vectors
    ini_vals(0,0) = x;
    ini_vals(1,0) = y;
    
    jacb(0,0)=dfdx;
    jacb(0,1)=dfdy;
    jacb(1,0)=dgdx;
    jacb(1,1)=dgdy;
    inv_jacob = Inverse(jacb);
    
    //Newtons Method
	x1 = ini_vals(0,0) - (inv_jacob(0,0)*f + inv_jacob(0,1)*g); 
	y1 = ini_vals(1,0) - (inv_jacob(1,0)*f + inv_jacob(1,1)*g);
	cout << i << " " << x1 <<  " " << y1<< "\n";
		
	if(fabs(x-x1) < epsilon && fabs(y-y1) < epsilon) 
	{
	  cout << "\n" << "The  x root is " << x1 << "."; //displays the root
	  cout << "\n" << "The  y root is " << y1 << "."<<"\n"; //displays the root
	  return 0;
    }
	

  }
  
  
    
		
	
	return 0;
}
