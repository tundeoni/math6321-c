/* Main routine to test a set of LMM  
     lambda*y + (1.0/(1.0 + t*t))- (lambda)*atan(t)

   Babatunde Oni */

#include <iostream>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"
#include "bwd_euler.hpp"
#include "lmm.hpp"
#include "trapezoidal.hpp"

using namespace std;


// Define classes to compute the ODE RHS function and its Jacobian

//    ODE RHS function class -- instantiates a RHSFunction
class MyRHS: public RHSFunction {
public:
  double lambdas;                                                    // stores some local data
  int Evaluate(double t, vector<double>& y, vector<double>& f) {    // evaluates the RHS function, f(t,y)
    f[0] = lambdas*y[0] + (1.0/(1.0 + t*t))- (lambdas)*atan(t);
    return 0;
  }
};

//    ODE RHS Jacobian function class -- instantiates a RHSJacobian
class MyJac: public RHSJacobian {
public:
  double lambdas;                                            // stores some local data
  int Evaluate(double t, vector<double>& y, Matrix& J) {    // evaluates the RHS Jacobian, J(t,y)
    J = 0.0;
    J(0,0) = lambdas;
    return 0;
  }
};


// Convenience function for analytical solution
vector<double> ytrue(const double t) { 
  vector<double> yt = {atan(t)};
  return yt;
};



// main routine
int main() {

  // time steps to try
  vector<double> h = {0.1, 0.01, 0.001, 0.0001};

  // lambda value
  vector<double> lambdas = {-10,-100,-1000,-10000};
  // set problem information
  vector<double> y0 = {0.0};
  long int M=1;
  double t0 = 0.0;
  double Tf = 3.0;
  double dtout = 0.3;

  // create ODE RHS and Jacobian objects, store lambda value for this test
  MyRHS rhs;
  MyJac Jac;
  
  vector<double> errs(h.size());
  
  
   
  for (int il=0; il<lambdas.size(); il++) {
    cout << "\n lambda "<< lambdas[il]<<" for AM2 & BDF3:";
  // set current lambda value into rhs and Jac objects
    rhs.lambdas = lambdas[il];
    Jac.lambdas = lambdas[il]; 
  
  
 
  
  cout << "\n AM-2:\n";
  // create LMM method, set solver parameters
  vector<double> AM1_a = {1.0, 0.0};
  vector<double> AM1_b = {5.0/12.0, 2.0/3.0, -1.0/12.0};
  LMMStepper AM1(rhs, Jac, y0, AM1_a, AM1_b);
  AM1.newt->SetTolerances(1.e-9, 1.e-11);
  AM1.newt->SetMaxit(20);
  
  
  
  
  for (int ih=0; ih<h.size(); ih++) {
    cout << "\n h = " << h[ih];

    // set the initial time
    double tcur = t0;
    
    double tout = t0 + dtout;

    // reset maxerr
    double maxerr = 0.0;
     
    // AM-1 only requires a single initial condition
    Matrix y_AM1(M,2);
    y_AM1[0] = y0;
    y_AM1[1] = y0;

    TrapezoidalStepper Tr(rhs, Jac, y0);
    Tr.newt->SetTolerances(1.e-9, 1.e-11);
    Tr.newt->SetMaxit(20);
    vector<double> tspan_tr = {t0, t0+h[ih]};
    vector<double> tvals_tr = Tr.Evolve(tspan_tr, h[ih], y_AM1[0]);
    //   update tcur to end of initial conditions
    tcur += h[ih];



    // loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
     tspan_tr = {tcur, tout};

      // call the solver, update current time
      tvals_tr = AM1.Evolve(tspan_tr, h[ih], y_AM1);
      tcur = tvals_tr.back();   // last entry in tvals

      // compute the error at tcur, output to screen and accumulate maximum
      vector<double> yerr = y_AM1[0] - ytrue(tcur);   // computed solution is in 1st column of y_AM1
      double err = InfNorm(yerr);
      maxerr = std::max(maxerr, err);
      
      tout = std::min(tcur + dtout, Tf);

    }
    cout << "\t Max error = " << maxerr << endl;
    
    errs[ih] = maxerr;
    
    if (ih > 0)
          cout << "\tConv = " << (log(errs[ih])-log(errs[ih-1]))/(log(h[ih])-log(h[ih-1]));
        cout << endl;
  }
  
  
    
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  ////////// BDF-3 //////////
  cout << "\n BDF-3:";

  // create backwards Euler and LMM methods, set solver parameters
  
  vector<double> BDF3_a = {18.0/11.0, -9.0/11.0, 2.0/11.0};
  vector<double> BDF3_b = {6.0/11.0, 0.0, 0.0, 0.0};
  
  LMMStepper BDF3(rhs, Jac, y0, BDF3_a, BDF3_b);
 
  BDF3.newt->SetTolerances(1.e-9, 1.e-11);
  BDF3.newt->SetMaxit(20);

  // loop over time step sizes
  for (int ih=0; ih<h.size(); ih++) {
    cout << "\n  h = " << h[ih];

    // set the initial time, first output time
    double tcur = t0;
    double tout = t0 + dtout;

    // reset maxerr
    double maxerr = 0.0;
     
    // BDF-3 requires three initial conditions
    Matrix y_BDF3(M,3);
    //   first two is just y0 (insert into 2nd & 3rd columns of y_BDF3)
    y_BDF3[2] = y0;
    //   first comes from trap step (insert into 1st column of y_BDF2)
    y_BDF3[1] = y0;
    
    //trap initial condition
    TrapezoidalStepper Tr(rhs, Jac, y0);
    vector<double> tspan_tr = {tcur, tcur+h[ih]};
    vector<double> tvals_tr = Tr.Evolve(tspan_tr, h[ih], y_BDF3[1]);
    //   update tcur to end of initial conditions
    tcur += h[ih];
    
    y_BDF3[0] = y_BDF3[1];
    tspan_tr = {t0 + h[ih] , t0 + 2*h[ih]};
    tvals_tr = Tr.Evolve(tspan_tr, h[ih], y_BDF3[0]);
    tcur += h[ih];

    // loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
      vector<double> tspan = {tcur, tout};

      // call the solver, update current time
      vector<double> tvals = BDF3.Evolve(tspan, h[ih], y_BDF3);
      tcur = tvals.back();   // last entry in tvals

      // compute the error at tcur, output to screen and accumulate maximum
      vector<double> yerr = y_BDF3[0] - ytrue(tcur);   // computed solution is in 1st column of y_BDF3
      double err = InfNorm(yerr);
      maxerr = std::max(maxerr, err);

      // update output time for next solve
      tout = std::min(tcur + dtout, Tf);

    }
    cout << "\t Max err =  " << maxerr << endl;
    errs[ih] = maxerr;
    
    if (ih > 0)
          cout << "\t Conv = " << (log(errs[ih])-log(errs[ih-1]))/(log(h[ih])-log(h[ih-1]));
        cout << endl;
  }
}
  return 0;
}
