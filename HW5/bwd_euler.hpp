/* Backward Euler time stepper class header file.

   D.R. Reynolds
   Math 6321 @ SMU
   Fall 2016  */

#ifndef BACKWARD_EULER_DEFINED__
#define BACKWARD_EULER_DEFINED__

// Inclusions
#include <vector>
#include <math.h>
#include "matrix.hpp"
#include "rhs.hpp"
#include "resid.hpp"
#include "newton.hpp"


// Backward Euler residual function class -- implements a 
// backward-Euler-specific ResidualFunction to be supplied 
// to the Newton solver.
class BEResid: public ResidualFunction {
public:

  // data required to evaluate backward Euler nonlinear residual
  RHSFunction *frhs;            // pointer to ODE RHS function
  double t;                     // current time
  double h;                     // current step size
  std::vector<double> *yold;    // pointer to solution at old time step

  // constructor (sets RHSFunction and old solution pointers)
  BEResid(RHSFunction& frhs_, std::vector<double>& yold_) {
    frhs = &frhs_;  yold = &yold_;
  };

  // residual evaluation routine
  int Evaluate(std::vector<double>& y, std::vector<double>& resid) {

    // evaluate RHS function (store in resid)
    int ierr = frhs->Evaluate(t+h, y, resid);
    if (ierr != 0) {
      std::cerr << "Error in ODE RHS function = " << ierr << "\n";
      return ierr;
    }

    // combine pieces to fill residual, y-yold-h*f(t+h,y)
    resid = y - (*yold) - h*resid;

    // return success
    return 0;
  }
};



// Backward Euler residual Jacobian function class -- implements 
// a backward-Euler-specific ResidualJacobian to be supplied 
// to the Newton solver.
class BEResidJac: public ResidualJacobian {
public:

  // data required to evaluate backward Euler residual Jacobian
  RHSJacobian *Jrhs;   // ODE RHS Jacobian function pointer
  double t;            // current time
  double h;            // current step size

  // constructor (sets RHS Jacobian function pointer)
  BEResidJac(RHSJacobian &Jrhs_) { Jrhs = &Jrhs_; };

  // Residual Jacobian evaluation routine
  int Evaluate(std::vector<double>& y, Matrix& J) {

    // evaluate RHS function Jacobian, Jrhs (store in J)
    int ierr = Jrhs->Evaluate(t+h, y, J);
    if (ierr != 0) {
      std::cerr << "Error in ODE RHS Jacobian function = " << ierr << "\n";
      return ierr;
    }
    // combine pieces to fill residual Jacobian,  J = I - h*Jrhs
    J *= (-h);
    for (int i=0; i<J.Rows(); i++)
      J(i,i) += 1.0;

    // return success
    return 0;
  }
};



// Backward Euler time stepper class
class BackwardEulerStepper {

 private:

  // private reusable local data
  std::vector<double> yold;   // old solution vector
  BEResid *resid;             // backward Euler residual function pointer
  BEResidJac *residJac;       // backward Euler residual Jacobian function pointer

 public:

  NewtonSolver *newt;   // Newton nonlinear solver pointer

  // constructor (constructs residual, Jacobian, and copies y for local data)
  BackwardEulerStepper(RHSFunction& frhs_, RHSJacobian& Jrhs_, 
                       std::vector<double>& y_);

  // destructor (frees pointers to local objects)
  ~BackwardEulerStepper() {
    delete resid;
    delete residJac;
    delete newt;
  };

  // Evolve routine (evolves the solution via backward Euler)
  std::vector<double> Evolve(std::vector<double>& tspan, double h, 
                             std::vector<double>& y);

};

#endif
