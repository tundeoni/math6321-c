
/* Forward Euler Method Test
     y' = f(t,y), t in [0,5],
     y(0) = y0.

   Babatunde Oni
   Math 6321 @ SMU
   Fall 2016  */

#include <iostream>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"
#include "fwd_euler.hpp"

using namespace std;



//    ODE RHS function class -- instantiates a RHSFunction
class RHS1: public RHSFunction {
public:
  int Evaluate(double t, vector<double>& y, vector<double>& f) {    // evaluates the RHS function, f(t,y)
    f = -exp(-t)*y;
    return 0;
  }
};
//    Convenience function for analytical solution
vector<double> ytrue1(const double t) { 
  vector<double> yt(1);
  yt[0] = exp(exp(-t)-1.0);
  return yt;
};

vector<double> h = {0.2, 0.1, 0.05, 0.025, 0.0125};




// main routine
int main() {

  // time steps to try
 

  // set problem information
  vector<double> y0_1 = {1.0};
  vector<double> all_max (h.size());
  double t0 = 0.0;
  double Tf = 5.0;
  double tcur = t0;
  double dtout = 1;

  // create ODE RHS function objects
  RHS1 f1;


  // create forward Euler stepper object
  ForwardEulerStepper FE1(f1, y0_1);


  // loop over time step sizes
  for (int ih=0; ih<h.size(); ih++) {

    // problem 1:
    vector<double> y = y0_1;
    tcur = t0;
    double maxerr = 0.0;
    cout << "\nRunning problem 1 with stepsize h = " << h[ih] << ":\n";

    //   loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
      vector<double> tspan = {tcur, std::min(tcur + dtout, Tf)};

      // call the solver, update current time
      vector<double> tvals = FE1.Evolve(tspan, h[ih], y);
      tcur = tvals.back();   // last entry in tvals

      // compute the error at tcur, output to screen and accumulate maximum
      vector<double> yerr = y - ytrue1(tcur);
      double err = InfNorm(yerr);
      maxerr = std::max(maxerr, err);
      cout << "  y(" << tcur << ") = " << y[0]
	   << "  \t||error|| = " << err
	   << endl;
    }
    
    all_max[ih]=maxerr;
    
    cout<< "Maximum Error stored " << all_max[ih]<<endl;
    cout << all_max<<endl;
  }

  return 0;
}
