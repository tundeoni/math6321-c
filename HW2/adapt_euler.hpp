/* Babatunde Oni
Mathematics 6321
Fall 2016 

This file defines the Adaptive Euler class. Here,
"vectors" are considered as std::vector<double> objects. 

*/

#ifndef FORWARD_EULER_DEFINED__
#define FORWARD_EULER_DEFINED__

// Inclusions
#include <iostream>
#include <ostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>

class adapt_euler{
//AdaptEuler(RHSFunction& frhs, double rtol,  double atol, std::vector<double>& y);

std::vector<double> Evolve(std::vector<double>& tspan, std::vector<double>& y);

 private:

  // private reusable local data
  std::vector<double> f;   // storage for ODE RHS vector
  RHSFunction *frhs;       // pointer to ODE RHS function

 public:

  // constructor (sets RHS function pointer, copies y for local data, and defines re)
  AdaptEuler(RHSFunction& frhs, double rtol,  double atol, std::vector<double>& y); {
    frhs = &frhs_;
    f = y;
  };

  // Evolve routine (evolves the solution via forward Euler)
  std::vector<double> Evolve(std::vector<double> tspan, 
                             std::vector<double>& y);

};

#endif
