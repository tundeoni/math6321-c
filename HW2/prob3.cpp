
/* Forward Euler Method Test
     y' = f(t,y), 
     f=exp(exp(-t)-1)
     y(0) = y0.

   Babatunde Oni
   Math 6321 @ SMU
   Fall 2016  */

#include <iostream>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"
#include "fwd_euler.hpp"

using namespace std;


// Problem 1:  y' = -exp(-t)y
//    ODE RHS function class -- instantiates a RHSFunction
class RHS1: public RHSFunction {
public:
  int Evaluate(double t, vector<double>& y, vector<double>& f) { 
// evaluates the RHS function, f(t,y)
    f = -exp(-t)*y;
    return 0;
  }
};
//    Convenience function for analytical solution
vector<double> ytrue1(const double t) { 
  vector<double> yt(1);
  yt[0] = exp(exp(-t)-1.0);
  return yt;
};






// main routine
int main() {

  // time steps to try
  

  // set problem information
  vector<double> y0_1 = {1.0};
  vector<double> all_max (h.size());
  double t0 = 0.0;
  double Tf = 5.0;
  double tcur = t0;
  double dtout = 1;
  vector<double> rtol={0.01,0.0001,0.000001,0.00000001};
  double atol= 0.00000000001;

  // create ODE RHS function objects
  RHS1 f1;


  // create forward Euler stepper object
  adapt_euler FE1(f1, y0_1);


  // loop over time step sizes
  h=0.1;

    // problem 1:
    vector<double> y = y0_1;
    tcur = t0;
    double maxerr = 0.0;
    cout << "\nRunning problem 1 with stepsize h = " << h << ":\n";

    //   loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
      vector<double> tspan = {tcur, std::min(tcur + dtout, Tf)};

      // call the solver, update current time
      vector<double> tvals = FE1.Evolve(tspan,y);
      tcur = tvals.back();   // last entry in tvals

      // compute the error at tcur, output to screen and accumulate maximum
      vector<double> yerr = y - ytrue1(tcur);
      if(fabs(yerr< (rtol*ytrue1(tcur) +atol){
      double err = InfNorm(yerr);
      maxerr = std::max(maxerr, err);
      cout << "  y(" << tcur << ") = " << y[0]
	   << "  \t||error|| = " << err
	   << endl;
	   h=h/2.0;
   }
    
   
  
   
  }

  return 0;

}
