
/* Adaptive Forward Euler time stepper class implementation file.

   Class to perform time evolution of the IVP
        y' = f(t,y),  t in [t0, Tf],  y(t0) = y0
   using an adaptive forward Euler time stepping method. 

   Babatunde Oni
   Math 6321 @ SMU
   Fall 2016 */


#include <vector>
#include "matrix.hpp"
#include "adapt_euler.hpp"


class RHS1: public RHSFunction {
public:
  int Evaluate(double t, vector<double>& y, vector<double>& f) {    // evaluates the RHS function, f(t,y)
    f = -1.0*y;
    return 0;
  }
};
//    Convenience function for analytical solution
vector<double> ytrue1(const double t) { 
  vector<double> yt(1);
  yt[0] = exp(-t);
  return yt;
};

std::vector<double> adapt_euler::Evolve(std::vector<double>& tspan, std::vector<double>& y){



double h=0.1;


// initialize output
  std::vector<double> times = {tspan[0]};

  // check for legal inputs 
  if (h <= 0.0) {
    std::cerr << "ForwardEulerStepper: Illegal h\n";
    return times;
  }
  if (tspan[1] <= tspan[0]) {
    std::cerr << "ForwardEulerStepper: Illegal tspan\n";
    return times;	  
  }
  
  
  }

  return times;
 }



















}
