/* Explicit Adams-Bashforth time stepper class header file.

   Babatunde Oni
   Math 6321 @ SMU
   Fall 2016  */

#ifndef AB1_DEFINED__
#define AB1_DEFINED__

// Inclusions
#include <math.h>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"


// Explicit AB1 time stepper class
class AB3Stepper {

 private:

  RHSFunction *frhs;            // pointer to ODE RHS function
  std::vector<double> f, f1,f2,f3;  // reused vectors
  
 public:

  // constructor (sets RHS function pointer, allocates local data)
  AB3Stepper(RHSFunction& frhs_, std::vector<double>& y) { 
    frhs = &frhs_;      // store RHSFunction pointer
    f = y;             // allocate reusable data
    f1= y;
    f2= y; 
    f3= y;
  };

  // Evolve routine (evolves the solution)
  std::vector<double> Evolve(std::vector<double>& tspan, double h, 
                             std::vector<double>& y, 
			     std::vector<double>& y1,
                             std::vector<double>& y2,
                             std::vector<double>& y3);

};

#endif
