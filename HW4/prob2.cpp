/* Main routine to test the  Fourth-order Adams 
   Bashforth method on the scalar-valued ODE problem 
     y' = -exp(-t)*y, t in [0,1],
     y(0) = 1.

   Babatunde Oni
   Math 6321 @ SMU
   Fall 2016  */

#include <iostream>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"
#include "erk4.hpp"
#include "ab3.hpp"

using namespace std;


// ODE RHS function
class MyRHS: public RHSFunction {
public:
  int Evaluate(double t, vector<double>& y, vector<double>& f) {
    f[0] = -exp(-t)*y[0];
    return 0;
  }
};


// Convenience function for analytical solution
vector<double> ytrue(const double t) { 
  vector<double> yt = {exp(exp(-t)-1.0)};
  return yt;
};




// main routine
int main() {

  // time steps to try
  vector<double> h = {0.2, 0.1, 0.05, 0.01, 0.05,0.001};

  // set problem information
  vector<double> y0 = {1.0};
  double t0 = 0.0;
  double Tf = 5.0;
  double dtout = 1;

  // create ODE RHS function objects
  MyRHS rhs;
 

  //AB RK stepper objects
  ERK4Stepper ERK4(rhs, y0);
  AB3Stepper AB3(rhs, y0);

  // storage for error results
  vector<double> errs(h.size());



  ///////// Adams Bashforth 3 /////////
  cout << "\nAdams Bashforth 3 Method:\n";

  // loop over time step sizes
  for (int ih=0; ih<h.size(); ih++) {
  // reset maxerr
  double maxerr = 0.0;
    
// set the initial conditions, initial time
    vector<double> ytemp(y0);
    
    ERK4.Step(t0,h[ih],ytemp);
    vector<double> y2 = ytemp;
    
    ERK4.Step(t0+h[ih],h[ih], ytemp);
    vector<double> y1=ytemp;
    
    ERK4.Step(t0+2.0*h[ih],h[ih], ytemp);
    vector<double> y=ytemp;

    vector<double> y3=y0;

    double tcur = t0;
 

  
     
    // loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
      vector<double> tspan = {tcur, std::min(tcur + dtout, Tf)};
      if (tcur<0.5){

      tspan[0] = t0+3.0*h[ih];
       }
      // call the solver, update current time
      vector<double> tvals = AB3.Evolve(tspan, h[ih], y, y1,y2,y3);
      tcur = tvals.back();     // last entry in tvals

      // compute the error at tcur, output to screen and accumulate maximum
      vector<double> yerr = y - ytrue(tcur);
      double err = InfNorm(yerr);
      maxerr = std::max(maxerr, err);
    }
    cout << "   h = " << h[ih] << "\t  error = " << maxerr;
    errs[ih] = maxerr;
    if (ih > 0)
      cout << "\t  conv rate = " << (log(errs[ih])-log(errs[ih-1]))/(log(h[ih])-log(h[ih-1]));
    cout << endl;
  }


  return 0;
}
