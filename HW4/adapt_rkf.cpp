/* Adaptive RKF class
 * 
 * BABATUNDE ONI
 * 
   Fall 2016  */
#include <vector>
#include "matrix.hpp"
#include "adapt_rkf.hpp"



// Single step of Runge-Kutta- Felhberg
//
// Inputs:  t holds the current time
//          h holds the current time step size
//          z, f0-f5 hold temporary vectors needed for the problem
//          y holds the current solution
// Outputs: y holds the updated solution, y(t+h)
//
// The return value is an integer indicating success/failure,
// with 0 indicating success, and nonzero failure.
int AdaptRKF::Step(double t, double h, std::vector<double>& y1, std::vector<double>& y2) {

  // stage 1: set stage and compute RHS
  z = y1;
  if (frhs->Evaluate(t, z, f0) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // stage 2: set stage and compute RHS
  z = y1 + (h*A(0,0))*f0;
  if (frhs->Evaluate(t+b[0]*h, z, f1) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // stage 3: set stage and compute RHS
  z = y1 + h*(A(1,0)*f0 + A(1,1)*f1);
  if (frhs->Evaluate(t+b[1]*h, z, f2) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // stage 4: set stage and compute RHS
  z = y1 + h*(A(2,0)*f0 + A(2,1)*f1 + A(2,2)*f2);
  if (frhs->Evaluate(t+b[2]*h, z, f3) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }
  
  // stage 5: set stage and compute RHS
  z = y1 + h*(A(3,0)*f0 + A(3,1)*f1 + A(3,2)*f2 + A(3,3)*f3);
  if (frhs->Evaluate(t+c[3]*h, z, f4) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }
  // stage 6: set stage and compute RHS
  z = y1 + h*(A(4,0)*f0 + A(4,1)*f1 + A(4,2)*f2+A(4,3)*f3+A(4,4)*f4);
  if (frhs->Evaluate(t+c[4]*h, z, f5) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // compute next step solution
  
  y2 = y1 + h*(c[0]*f0 + c[1]*f1 + c[2]*f2 + c[3]*f3 +c[4]*f4+c[5]*f5);
  y1 += h*(d[0]*f0 + d[1]*f1 + d[2]*f2 + d[3]*f3 +d[4]*f4);
  
  // return success
  return 0;
}


// The adaptive rkf evolution routine
//
// Inputs:  tspan holds the current time interval, [t0, tf]
//          y holds the initial condition, y(t0)
// Outputs: y holds the computed solution, y(tf)
//
// The return value is a row vector containing all internal 
// times at which the solution was computed,
//               [t0, t1, ..., tN]
std::vector<double> AdaptRKF::Evolve(std::vector<double>& tspan, 
                                       std::vector<double>& y) {
  fails = 0;
  steps = 0;
  // initialize output
  std::vector<double> tvals = {tspan[0]};

  // check for positive tolerances
  if ((rtol <= 0.0) || (atol <= 0.0)) {
    std::cerr << "Evolve error: illegal tolerances, atol = " 
	      << atol << ",  rtol = " << rtol << std::endl;
    return tvals;
  }

  // get |y'(t0)|
  if (frhs->Evaluate(tspan[0], y, fn) != 0) {
    std::cerr << "Evolve error in RHS function\n";
    return tvals;
  }

  // estimate initial h value via linearization, safety factor
  error_norm = std::max(InfNorm(fn) / ( rtol * InfNorm(y) + atol ), 1.e-8);
  h = safe/error_norm;
  if (tspan[0]+h > tspan[1])   h = tspan[1]-tspan[0];

  // iterate over time steps (all but the last one)
  long int cursteps=0;
  for (int tstep=1; tstep<=maxit; tstep++) {

    // reset both solution approximations to current solution
    y1 = y;
    y2 = y;

    // March
    if (Step(tvals[steps], h, y1, y2) != 0) {
      std::cerr << "Evolve error in RHS function\n";
      return tvals;
    }
  

   
    // compute error estimate
    yerr = y2 - y1;

    // compute error estimate success factor
    error_norm = std::max(InfNorm(yerr) / ( rtol * InfNorm(y1) + atol ), 1.e-8);

    // if solution has too much error: reduce step size, increment failure counter, and retry
    if (error_norm > ONEPSM) {
      h *= fail;
      fails++;
      continue;
    }

    // successful step
    tvals.push_back(tvals[cursteps++] + h);  // append updated time, increment step counter
    y = y1;
    steps++;

    // exit loop if we've reached the final time
    if ( tvals[cursteps] >= tspan[1]*ONEMSM )  break;

    // pick next time step size based on this error estimate
    double eta = safe*std::pow(error_norm, alpha);    // step size estimate
    eta = std::min(eta, grow);                        // maximum growth
    h *= eta;                                         // update h
    h = std::min(h, tspan[1]-tvals[cursteps]);        // don't pass Tf

  }

  return tvals;
}

AdaptRKF::AdaptRKF(RHSFunction& frhs_, double rtol_, 
                       double atol_, std::vector<double>& y) {
  frhs = &frhs_;    // set RHSFunction pointer
  rtol = rtol_;     // set tolerances
  atol = atol_;
  fn = y;           // clone y to create local vectors
  y1 = y;
  y2 = y;
  yerr = y;

  maxit = 1e6;      // set default solver parameters
  grow = 50.0;
  safe = 0.95;
  fail = 0.5;
  ONEMSM = 1.0 - 1.e-8;
  ONEPSM = 1.0 + 1.e-8;
  alpha = -0.2;
  fails = 0;
  steps = 0;
  error_norm = 0.0;
  h = 0.0;
  
    frhs = &frhs_;                         // store RHSFunction pointer
    z  = y;                                // allocate reusable data 
    f0 = y;                                //   based on size of y
    f1 = y;
    f2 = y;
    f3 = y;
    f4 = y;
    f5 = y;
    
    // Butcher table data for the RKF
    A = Matrix(5,5);                      
    b= {1.0/4.0, 3.0/8.0, 12.0/13.0, 1.0, 5.0};    
    A(0,0) = 1.0/4.0;
    A(1,0) = 3.0/32.0;
    A(1,1) = 9.0/32.0;
    A(2,0) = 1932.0/2197.0;
    A(2,1) = -7200.0/2197.0;
    A(2,2) = 7296.0/2197.0;
    A(3,0) = 439.0/216.0;
    A(3,1) = -8.0;
    A(3,2) = 3680.0/513.0;
    A(3,3) = -845.0/4104.0;
    A(4,0) = -8.0/27.0; 
    A(4,1) =  2.0; 
    A(4,2) = -3544.0/2565.0;
    A(4,3) = 1859.0/4104.0; 
    A(4,4) = -11.0/40.0;
    
    c = {16.0/135.0,0.0, 6656.0/12825.0, 28561.0/56430.0,-9.0/50.0,2.0/55.0};
    d = {25.0/216.0,0.0, 1408.0/2565.0, 2197.0/4104.0,-1.0/5.0, 0.0};
  
}
