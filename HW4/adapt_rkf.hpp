/* Adaptive RKF header file
 * 
 * BABATUNDE ONI
 * 
   Fall 2016  */
#ifndef ADAPT_RKF_DEFINED__
#define ADAPT_RKF_DEFINED__

// Inclusions
#include <vector>
#include <math.h>
#include "matrix.hpp"
#include "rhs.hpp"


// Adaptive forward Euler time stepper class
class AdaptRKF	{

 private:

  // private reusable local data
  RHSFunction *frhs;          // pointer to ODE RHS function
  std::vector<double> fn;     // local vector storage
  std::vector<double> y1;
  std::vector<double> y2;
  std::vector<double> yerr;
                                 
  std::vector<double> z, f0, f1, f2, f3, f4, f5;    // reused vectors
  Matrix A;                                         // Butcher table
  std::vector<double> b, c, d;
  

 public:

  double rtol;        // desired relative solution error
  double atol;        // desired absolute solution error
  double grow;        // maximum step size growth factor
  double safe;        // safety factor for step size estimate
  double fail;        // failed step reduction factor
  double ONEMSM;      // safety factors for
  double ONEPSM;      // floating-point comparisons
  double alpha;       // exponent relating step to error
  double error_norm;  // current estimate of the local error ratio
  double h;           // current time step size
  long int fails;     // number of failed steps
  long int steps;     // number of successful steps
  long int maxit;     // maximum number of steps

  // constructor (sets RHS function pointer & solver parameters, copies y for local data)
   AdaptRKF(RHSFunction& frhs, double rtol, double atol, std::vector<double>& y); 
  
  
  // Evolve routine (evolves the solution via adaptive Runge Kutta Fehlberg)
   std::vector<double> Evolve(std::vector<double>& tspan, std::vector<double>& y);
   
   // Single step calculation
  int Step(double t, double h, std::vector<double>& y1, std::vector<double>& y2);



};
#endif
