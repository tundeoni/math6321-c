/*
    

   Babatunde Oni
   Math 6321 @ SMU
   Fall 2016  */

#include <iostream>
#include <iomanip>
#include <vector>
#include "rhs.hpp"
#include "matrix.hpp"
#include "adapt_euler.hpp"

using namespace std;


// ODE RHS function class -- instantiates a RHSFunction
//   includes extra routines to set up the problem and 
//   evaluate the analytical solution
class ODESystem: public RHSFunction {
 
public:
  
  
  // evaluates the RHS function, f(t,y)
  int Evaluate(double t, vector<double>& y, vector<double>& f) {
    f[0]= -3.0*y[0]+y[1]-exp(-2*t);
    f[1]= y[0]-3*y[1]+exp(-t);
    return 0;
  }
  // computes the true solution, y(t)
  vector<double> TrueSolution(double t, vector<double>& y0) {
  std::vector<double> yt(2);
    yt[0] = 2.0*exp(-2*t)- (t*exp(-2*t)) + exp(-t);
    yt[1] = (11/12)	*exp(-4*t)-exp(-2*t)-(2.0/3.0)*exp(-t);
  }
};



// main routine
int main(int argc, char **argv) {

  // get problem size from command line, otherwise set to 5
  int N = 5;
  if (argc > 1)
    N = atoi(argv[1]);
  cout << "\nRunning system ODE problem with N = " << N << endl;

  // set up problem
  ODESystem MyProblem;
  
  double t0 = 0.0;
  double Tf = 1.0;
  double tcur = 0.0;
  double dtout = 0.1;

  // solver tolerances
  vector<double> rtols = {1.e-2, 1.e-4, 1.e-6,1.e-8};
  double atol = 1.e-12;

  // initial condition
  vector<double> Y0 = Random(N);

  // statistics variables
  long int totsteps=0, totfails=0;

  // create forward Euler solver object (will reset rtol before each solve)
  AdaptEuler AE(MyProblem, 0.0, atol, Y0);

  // loop over tolerances
  for (int ir=0; ir<rtols.size(); ir++) {

    // set the initial condition, initial time
    vector<double> Y(Y0);
    tcur = t0;

    // reset maxerr
    double maxabserr = 0.0;
    double maxrelerr = 0.0;

    AE.rtol = rtols[ir];
    AE.atol = atol;
    cout << "\nRunning with tolerances rtol = " << AE.rtol 
	 << ", atol = " << AE.atol << endl;
	 //vector<double> rtol_y_atol[ir] = (AE.rtol[ir]*Y) +AE.atol;
	 //cout<< "rtol times y +atol" << rtol_y_atol[ir] << endl;

    // loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
      vector<double> tspan = {tcur, std::min(tcur + dtout, Tf)};

      // call the solver, update current time
      vector<double> tvals = AE.Evolve(tspan, Y);
      tcur = tvals.back();   // last entry in tvals

      // compute the error at tcur and output to screen
      vector<double> Ytrue = MyProblem.TrueSolution(tcur,Y0);
      vector<double> Yerr = Y - Ytrue;
      double abserr = InfNorm(Yerr);
      double relerr = InfNorm(Yerr) / InfNorm(Ytrue);
      cout << " Y(" << tcur << ") =" << Y;

      // accumulate overall solver statistics
      maxabserr = std::max(maxabserr, abserr);
      maxrelerr = std::max(maxrelerr, relerr);
      totsteps += AE.steps;
      totfails += AE.fails;
    }

    cout << "Overall:  steps = " << totsteps << ",  fails = " << totfails 
	 << ",  abserr = " << maxabserr << ",  relerr = " << maxrelerr << endl;
	 
	 
  }

  return 0;
}
