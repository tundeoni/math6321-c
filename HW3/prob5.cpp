/*
 
   Babatunde Oni
   Math 6321 @ SMU
   Fall 2016  */

#include <iostream>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"
#include "trapezoidal.hpp"

using namespace std;


// Define classes to compute the ODE RHS function and its Jacobian

//    ODE RHS function class -- instantiates a RHSFunction
class MyRHS: public RHSFunction {
public:
  double lambda;                                    // stores some local data
  int Evaluate(double t, vector<double>& y, vector<double>& f) {    // evaluates the RHS function, f(t,y)
    f[0] = lambda*y[0] + (1.0/(1.0+(t*t)) - ((lambda)*atan(t)));
    return 0;
  }
};

//    ODE RHS Jacobian function class -- instantiates a RHSJacobian
class MyJac: public RHSJacobian {
public:
  double lambda;                                            // stores some local data
  int Evaluate(double t, vector<double>& y, Matrix& J) {    // evaluates the RHS Jacobian, J(t,y)
    J(0,0) = lambda;
    return 0;
  }
};


// Convenience function for analytical solution
vector<double> ytrue(const double t) { 
  vector<double> yt = {atan(t)};
  return yt;
};



// main routine
int main() {

  // time steps to try
  vector<double> h = {0.1, 0.01,0.001,0.0001};

  // lambda values to try
  vector<double> lambdas = {-200, -2000, -20000};
  vector<double> thetas={1,0.55,0.5,0.45};

  // set problem information
  vector<double> y0 = {0};
  vector<double> abserrs(h.size());
  double t0 = 0.0;
  double Tf = 1.0;
  double dtout = 0.1;

  // create ODE RHS and Jacobian objects
  MyRHS rhs;
  MyJac Jac;

  // create time stepper objects
  for (int it=0;it<thetas.size();it++){
  GeneralizedTrapezoid Tr(rhs, Jac, thetas[it],y0);
  // update Newton solver parameters
  
  Tr.newt->SetTolerances(1.e-9, 1.e-11);
  Tr.newt->SetMaxit(20);

  //------ Trapezoidal tests ------

  // loop over lambda values
  
  for (int il=0; il<lambdas.size(); il++) {
    
    // set current lambda value into rhs and Jac objects
    rhs.lambda = lambdas[il];
    Jac.lambda = lambdas[il];
    
    // loop over time step sizes
    for (int ih=0; ih<h.size(); ih++) {
      
      // set the initial condition, initial time
      vector<double> y(y0);
      double tcur = t0;

      // reset maxerr
      double maxerr = 0.0;
     
      cout << "\nRunning trapezoidal with stepsize h = " << h[ih] 
	   << ", lambda = " << lambdas[il] << ":\n"
           << ", theta = " << thetas[it] << ":\n"      ;

      // loop over output step sizes: call solver and output error
      while (tcur < 0.99999*Tf) {
      
	// set the time interval for this solve
	vector<double> tspan = {tcur, std::min(tcur + dtout, Tf)};

	// call the solver, update current time
	vector<double> tvals = Tr.Evolve(tspan, h[ih], y);
	tcur = tvals.back();   // last entry in tvals

	// compute the error at tcur, output to screen and accumulate maximum
	vector<double> yerr = y - ytrue(tcur);
	double err = InfNorm(yerr);
	maxerr = std::max(maxerr, err);
	cout << "  y(" << tcur << ") = " << y[0]
	     << "  \t||error|| = " << err
	     << endl;

    
  }  
    abserrs[ih] = maxerr;
    cout << "overall abserr = " << abserrs[ih] << endl;
    
    cout << "\nConvergence order estimates:\n";
    for (int ih=0; ih<h.size()-1; ih++) {
    double dlogh = log(h[ih+1]) - log(h[ih]);
    double dloge = log(abserrs[ih+1]) - log(abserrs[ih]);
    cout << "  order = " << dloge/dlogh << endl;
  }
    
    
    }
   } 
}
 return 0;
}
