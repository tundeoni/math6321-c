/* Trapezoidal time stepper class header file.

   Babatunde Oni
   Math 6321 @ SMU
   Fall 2016  */

#ifndef TRAPEZOIDAL_DEFINED__
#define TRAPEZOIDAL_DEFINED__

// Inclusions
#include <vector>
#include <math.h>
#include "matrix.hpp"
#include "rhs.hpp"
#include "resid.hpp"
#include "newton.hpp"



class TrapResid: public ResidualFunction {
public:

  // data required to evaluate trapezoid nonlinear residual
  RHSFunction *frhs;            // pointer to ODE RHS function
  double t;                     // current time
  double h;                     // current step size
  std::vector<double> *yold;    // pointer to solution at old time step
  std::vector<double>  fold;    // extra vector for residual evaluation
  double theta;

  // constructor (sets RHSFunction and old solution pointers)
  TrapResid(RHSFunction& frhs_, std::vector<double>& yold_,double theta_) {
    frhs = &frhs_;  yold = &yold_;  fold = yold_;theta =theta_;
  };

  // residual evaluation routine
  int Evaluate(std::vector<double>& y, std::vector<double>& resid) {

    // evaluate RHS function at new time (store in resid)
    int ierr = frhs->Evaluate(t+h, y, resid);
    if (ierr != 0) {
      std::cerr << "Error in ODE RHS function = " << ierr << "\n";
      return ierr;
    }

    // evaluate RHS function at old time
    ierr = frhs->Evaluate(t, (*yold), fold);
    if (ierr != 0) {
      std::cerr << "Error in ODE RHS function = " << ierr << "\n";
      return ierr;
    }

    // combine pieces to fill residual
    resid = y - (*yold) - ((1-theta)*h*fold)-(theta*h*resid);

    // return success
    return 0;
  }
};




class TrapResidJac: public ResidualJacobian {
public:

  // data required to evaluate backward Euler residual Jacobian
  RHSJacobian *Jrhs;   // ODE RHS Jacobian function pointer
  double t;            // current time
  double h;            // current step size
  double theta;

  // constructor (sets RHS Jacobian function pointer)
  TrapResidJac(RHSJacobian &Jrhs_, double theta_) { Jrhs = &Jrhs_; theta = theta_;};

  // Residual Jacobian evaluation routine
  int Evaluate(std::vector<double>& y, Matrix& J) {

    // evaluate RHS function Jacobian, Jrhs (store in J)
    int ierr = Jrhs->Evaluate(t+h, y, J);
    if (ierr != 0) {
      std::cerr << "Error in ODE RHS Jacobian function = " << ierr << "\n";
      return ierr;
    }
    // combine pieces to fill residual Jacobian,  J = I - h/2*Jrhs
    J *= (-theta*h);
    for (int i=0; i<J.Rows(); i++)
      J(i,i) += 1.0;

    // return success
    return 0;
  }
};



// Trapezoidal time stepper class
class GeneralizedTrapezoid{

 private:

  // private reusable local data
  std::vector<double> yold;   // old solution vector
  TrapResid *resid;           // trapezoidal Euler residual function pointer
  TrapResidJac *residJac;     // trapezoidal residual Jacobian function pointer

 public:

  NewtonSolver *newt;   // Newton nonlinear solver pointer

  // constructor (constructs residual, Jacobian, and copies y for local data)
  GeneralizedTrapezoid(RHSFunction& frhs_, RHSJacobian& Jrhs_, 
                     double theta, std::vector<double>& y_);

  // destructor (frees pointers to local objects)
  ~GeneralizedTrapezoid() {
    delete resid;
    delete residJac;
    delete newt;
  };

  // Evolve routine (evolves the solution via trapezoidal)
  std::vector<double> Evolve(std::vector<double>& tspan, double h, 
                             std::vector<double>& y);

};

#endif
